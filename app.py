import os
import re

import numpy as np
import pandas as pd
import textwrap
import yaml

from dash import Dash, html, dcc, callback, Output, Input, dash_table
import dash_daq as daq

import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots

from utils.graphics import (
    load_layout_config,
    load_traces_config,
    plot_lollipop
)

from utils.data_loader import (
    load_congressional_legislation,
    tabulate_bills,
    tabulate_tags
)


### 1. Define file paths ###########################################

home_dir = './'
plotly_layout_file = os.path.join(home_dir, 'assets/plotly/layout.yaml')
plotly_traces_file = os.path.join(home_dir, 'assets/plotly/traces.yaml')
citris_tracker_file = os.path.join(home_dir, 'data/AI Legislation-Grid view.csv')
us_state_grid_file = os.path.join(home_dir, 'data/us-state-tile-grid.csv')
description_file = os.path.join(home_dir, 'data/description.md')

### 2. General plot config #########################################

layout_cfg = load_layout_config(plotly_layout_file)
traces_cfg = load_traces_config(plotly_traces_file)


### 3. Preprocess data #############################################

status_order = {
    'Introduced': 0,
    'Passed First Chamber': 1,
    'Passed Other Chamber': 2,
    'Became Law': 3
}

status_update = {
    'US_116_HR6216': {
        'new_status': 'Introduced',
        'note': '- See [congress.gov](https://www.congress.gov/bill/116th-congress/house-bill/6216/all-actions)\n'
    },
    'US_117_HRES1512': {
        'new_status': 'Passed House',
        'note': '- Conflicting status:\n'\
            '  - [congress.gov](https://www.congress.gov/bill/117th-congress/house-resolution/1512/all-actions): *Agreed to in House* \n' \
            '  - [legiscan.com](https://legiscan.com/US/bill/HR1512/2021): *Introduced on December 8 2022 - 25% progression, died in chamber*\n'\
            '  - [openstates.org](https://openstates.org/us/bills/117/HRES1512/): *Passed House* (going with this)\n'\
            '  - Note however [govtrack.us](https://www.govtrack.us/congress/bills/117/hres1512): '\
            '* Agreed To (Simple Resolution) on Dec 8, 2022: '\
            'This simple resolution was agreed to on December 8, 2022. That is the end of the legislative process for a simple resolution.*'
    },
    'US_117_HR4521': {
        'new_status': 'Passed Senate',
        'note': '- This is based on [openstates.org](https://openstates.org/us/bills/117/HR4521/)\n'
    }
}

row_update = {
    'H.R.3369 - Artificial Intelligence Accountability Act': {
        'Congress': '118th Congress (2023-2024)',
        'Party Affiliation of Introducer': 'Democrat',
    },
    'S.1699 Tech to Save Moms Act': {
        'Website': 'https://www.congress.gov/bill/118th-congress/senate-bill/1699'
    },
    # 'H.R.5066 Tech to Save Moms Act': {
    #     'Focus Tags': 'Pensions,Labor,Education,Health',
    #     'note': 'The Focus Tags are copied from [US_118_S1699](https://www.congress.gov/bill/118th-congress/senate-bill/1699) because they are related bills and this one is empty'
    # },
    
}

cpl_df, data_updates = load_congressional_legislation(
    citris_tracker_file,
    row_update=row_update,
    status_order=status_order,
    status_update=status_update,
    fill_val_for_missing_bipartisan_support='No'
)

tag_index_df, tag_status_df = tabulate_tags(cpl_df)
all_tags = ['ALL'] + list(tag_index_df['tag'].unique())

us_tilegrid = pd.read_csv(us_state_grid_file, comment='#').add_prefix('state_')

with open(description_file, 'r') as f:
    desc_md = f.read()
    desc_md += '\n' + data_updates

### 4. Data specific configs #######################################

gen_cfg = dict(
    category_orders = {
        'status': np.array(list(status_order.keys())),
        'status_type': ['current_status', 'cumulative_status'],
        'current_status': np.array(list(status_order.keys())),
        'cumulative_status': np.array(list(status_order.keys())),
        'legislature_chamber': ['House', 'Senate'],
        'party': [ 'Bipartisan', 'Democrat', 'Republican' ]
    },
    labels={
        'bill_id': 'Bill ID',
        'party': 'Party support',
        'num_bills': '# bills',
        'created_date': 'Introduced date',
        'legislature_chamber': 'Introduced chamber',
        'current_status': 'Current status',
        'cumulative_status': 'Minimum status',
        'tag_hover': 'Bill Topic',
        'tag': 'Bill Topic',
        'tags': 'Bill Topic',
        'count': 'Count',
        'is_bipartisan': 'Has Bipartisan Support',
        'introducer_home_state': 'Introducer Home State',
        'unq_id': 'Unique ID',
        'bill_description':' Description',
        'session': 'Session'
    },
)


color_maps = dict(
    party = {
        'Republican': '#ca0020', #'#ca0020','#ef8a62'
        'Democrat': '#0571b0', #'#0571b0','#67a9cf'
        'Bipartisan': '#d1d1d1', #'#d7d7d7'
    },
    legislature_chamber = {
        'House': '#bfbfbf',
        'Senate': '#2a2a2a',
    },
    status_sequence = {
        'Introduced': '#d9d9d9',
        'Passed First Chamber': '#bdbdbd',
        'Passed Other Chamber': '#969696',
        'Became Law':'#252525',
    }
)


def cascade_cross_merge(variations):
    df = None
    for k, v in variations.items():
        _df_kv = pd.DataFrame({k:v})
        if df is None:
            df = _df_kv
            continue

        df = pd.merge(df, _df_kv, how='cross')
    return df

tmpl_tabulate_df = cascade_cross_merge({
    k: v for k, v in gen_cfg['category_orders'].items()
     if k in ['party', 'legislature_chamber', 'status', 'status_type']
})

display_df = cpl_df.filter([
    'bill_id',
    'session',
    'legislature_chamber',
    'party',
    'current_status',
    'bill_description',
    'created_date',
    'tags',
    'introducer_home_state',
    'unq_id',
    'website'
]).copy()
display_df['tags'] = display_df['tags'].apply(lambda x: ' | '.join(x))
display_df['created_date'] = display_df['created_date'].dt.strftime('%Y-%m-%d')
display_df['bill_id'] = display_df.apply(
    lambda x: f"<a href='{x['website']}' target='_blank'>{x['bill_id']}</a>",
    axis=1
)
display_df = display_df.drop(columns='website')


### 5. Functions for graphs ########################################

tag_table_indices = {
    tag.upper(): cpl_df['tags'].apply(
        lambda x: any([
            tag.upper() == 'ALL' ,
            tag in x
        ])
    )
    for tag in all_tags
}

def filter_by_tag(tag, df=None):
    if df is None:
        df = cpl_df
    has_tag = tag_table_indices[tag.upper()]
    df = df[has_tag].reset_index(drop=True)
    return df


@callback(
    Output(component_id='TOTAL_BILLS_BY_STATUS', component_property='figure'),
    Input(component_id='GLOBAL_TAG', component_property='value'),
)
def create_fig_bills_by_status(global_tag, reverse_xaxes = False, display_xoffset_scale = 0.05):
    # Calculate data
    plt_vars = ['party', 'legislature_chamber']
    plt_key = 'num_bills'

    df_tag = filter_by_tag(global_tag)

    df = pd.concat(
        [
            tabulate_bills(df_tag, plt_vars, source='current_status', metric_name=plt_key, template_df=tmpl_tabulate_df),
            tabulate_bills(df_tag, plt_vars, source='cumulative_status', metric_name=plt_key, template_df=tmpl_tabulate_df),
        ],
        ignore_index=True
    )

    df = pd.merge(df, tmpl_tabulate_df, how='outer')\
        .fillna({'num_bills': 0}).astype({'num_bills': int})

    df_sum = df.groupby(['status_type', 'legislature_chamber', 'status'])['num_bills'].sum().reset_index()
    max_xrange = df_sum['num_bills'].max()
    display_xoffset = display_xoffset_scale * max_xrange

    # Plot
    fig = px.bar(
        df,
        x="num_bills",
        y="status",
        color='party',
        facet_col='legislature_chamber',
        facet_row='status_type',
        facet_col_spacing=0.05,
        facet_row_spacing=0.15,
        barmode='stack',
        color_discrete_map=color_maps['party'],
        category_orders=gen_cfg['category_orders'],
        labels=gen_cfg['labels'],
    )

    fig.update_traces(hovertemplate= '%{x}')


    varcombo_subplot_map = {
        ('current_status', 'House'): (2, 1),
        ('cumulative_status', 'House'): (1, 1),
        ('current_status', 'Senate'): (2, 2),
        ('cumulative_status', 'Senate'): (1, 2),
    }
    annot_txt = ''
    for (st, lc), (r, c) in varcombo_subplot_map.items():
        is_st = df_sum['status_type'] == st
        is_lc = df_sum['legislature_chamber'] == lc
        pos_num = df_sum['num_bills'] > 0

        df_sel = df_sum[is_st & is_lc & pos_num]

        txt = [str(n) for n in df_sel['num_bills'].astype('int').to_list()]
        fig.add_trace(go.Scatter(
            mode='text',
            x=df_sel['num_bills'].to_numpy() + display_xoffset,
            y=df_sel['status'].to_numpy(),
            text=txt,
            textposition='middle right',
            showlegend=False,
            hoverinfo='none',
            name='Total',
            legendgroup='Total'
        ), row=r, col=c)

        if 'cum' in st:
            n_intro = df_sel.query('status == "Introduced"')['num_bills'].to_list()
            n_tolaw = df_sel.query('status == "Became Law"')['num_bills'].to_list()

            if len(n_intro) == 0:
                n_intro = 0
            else:
                n_intro = n_intro[0]

            if len(n_tolaw) == 0:
                n_tolaw = 0
            else:
                n_tolaw = n_tolaw[0]

            if n_intro == 0:
                txt_p_intro2law = ''
            else:
                p_intro2law = round(100 * (n_tolaw / n_intro))
                txt_p_intro2law = f' (<b>{p_intro2law}%</b>)'
            annot_txt += f'Of <b>{n_intro}</b> proposed in <b>{lc}</b><br><b>{n_tolaw}</b> Became Law{txt_p_intro2law}<br><br>'

    # Decorations

    fig.update_layout(layout_cfg)

    fig.for_each_annotation(
        lambda a: a.update(
            text=f'Introduced in <b>{txt[1]}</b>'
                if 'chamber' in (txt:=a.text.split("="))[0]
                else '<i>%s</i>' %(gen_cfg['labels'][txt[1]].upper()),
            font=dict(size=16),
            textangle=0,
            xanchor='center' if 'chamber' in txt[0] else 'right',
            yanchor='bottom' if 'chamber' in txt[0] else 'top',
            y=1.05 if 'chamber' in txt[0]
                else 0.1 if 'cum' in txt[1].lower()
                else 0.6,
            hovertext=None if 'chamber' in txt[0]
                else 'Bills that have <em>at least reached</em> a given legislative process' if 'cum' in txt[1].lower()
                else 'Bills that are <em>currently only at</em> a given legislative status'
        )
    )

    fig.update_xaxes(range=[0, max_xrange + display_xoffset * 4])

    rv_auto_rng = dict()
    if reverse_xaxes:
        fig.update_xaxes(matches=None)
        rv_auto_rng = {'autorange':'reversed'}

    fig.update_yaxes(title=None)


    fig.add_annotation(
        xref = 'paper',
        yref = 'paper',
        xanchor = 'left',
        align='right',
        x=1,
        y=0,
        text=annot_txt,
        showarrow = False,
        font={'size': 9},
        bordercolor='#111111',
        borderpad=2
    )

    fig.update_layout(
        height=350,
        width=750,
        margin={'pad':5},
        xaxis1={**rv_auto_rng},
        yaxis1={'showline':False,'ticks':None},
        xaxis3={'visible': False, **rv_auto_rng},
        yaxis3={'showline':False,'ticks':None},
        yaxis2={'visible': False},
        xaxis4={'visible': False},
        yaxis4={'visible': False},
        uniformtext_minsize=12,
        uniformtext_mode='hide',
        hovermode = 'y unified',
        spikedistance=0,
        legend_itemclick=False,
        legend_itemdoubleclick=False
    )


    return fig


@callback(
    Output(component_id='BILLS_BY_INTRODUCED_DATE', component_property='figure'),
    [
        Input(component_id='GLOBAL_TAG', component_property='value'),
        Input(component_id='BILLS_BY_INTRODUCED_DATE::status_type', component_property='value'),
        Input(component_id='BILLS_BY_INTRODUCED_DATE::color_by', component_property='value'),
        Input(component_id='BILLS_BY_INTRODUCED_DATE::cum_hist', component_property='on'),
        Input(component_id='BILLS_BY_INTRODUCED_DATE::show_stack', component_property='on'),
    ]
)
def create_fig_time_histogram(
    global_tag = 'ALL',
    status_type = 'current_status',
    color_by='party',
    cum_hist = False,
    show_stack = True
):
    vis_mode = 'stack' if show_stack else 'group'

    bin_size_days = 365 * 0.5
    time_range = [cpl_df['created_date'].min(), cpl_df['created_date'].max()]

    num_bins = None
    if global_tag.upper() == 'ALL':
        num_bins = (time_range[1] - time_range[0]).days / bin_size_days
        num_bins = int(num_bins * 2) # unclear why needs to times 2 here to get the right nbins of size

    df_viz = cpl_df
    if 'cum' in status_type:
        df_viz = cpl_df.explode(status_type)

    df_viz = filter_by_tag(global_tag, df_viz)

    fig = px.histogram(
        df_viz,
        x = 'created_date',
        color = color_by,
        nbins = num_bins,
        facet_col = status_type,
        cumulative = cum_hist,
        category_orders=gen_cfg['category_orders'],
        color_discrete_map=color_maps[color_by],
        labels=gen_cfg['labels'],
        facet_col_spacing=0.05,
        marginal="rug",
        hover_data={
            'bill_id': True,

            status_type: False,
            'legislature_chamber': False,
            'party': False,
            'created_date': False
        },
    )

    fig.for_each_annotation(
        lambda a: a.update(
            text=(txt:=a.text.split("="))[1],
            font=dict(size=15),
            textangle=0,
            yanchor='top',
            y=0.97
        )
    )

    fig.update_layout(layout_cfg)

    title_text = 'Distribution of Bill <b>%s</b> by Introduced Dates' %(
        status_type.replace('_',' ').title()
    )
    fig.update_yaxes(title=None)

    fig.update_layout(
        height=350,
        width=800,
        margin={'pad':5},
        title_text=title_text,
        title_font={'size':16},
        yaxis1={'showline':True,'ticks':None, 'matches':None},
        yaxis2={'showline':True,'ticks':None, 'matches':'y2'},
        yaxis3={'showline':True,'ticks':None, 'matches':'y2'},
        yaxis4={'showline':True,'ticks':None, 'matches':'y2'},
        **{'xaxis' + str(i): {'visible':False} for i in range(5,9)},
        **{'yaxis' + str(i): {'visible':False, 'domain': [0.75,0.88]} for i in range(5,9)},
        hoverlabel=dict(
            font_size = 12
        ),
        barmode=vis_mode,
        hovermode = 'x unified',
        spikedistance=0
    )

    return fig


def create_fig_tag_breakdown():

    fig = make_subplots(
        rows=2, cols=1,
        shared_xaxes=True,
        row_heights=(3,1),
        subplot_titles=[
            '<b>Bill Topics</b> by Partisan Support',
        ]
    )

    plot_lollipop(
        tag_index_df,
        x = 'tag',
        y = 'partisan_index',
        sort_by = 'partisan_index',
        ascending=True,
        # orientation = 'horizontal',
        hue = 'partisan_index',
        marker_size = 6,
        line_color='black',
        marker_offset=0.04,
        showscale=False,
        fig=fig,
        fig_kwargs=dict(rows=1, cols=1)
    )

    fig.add_hline(
        y=0,
        line={'color':'black', 'width': 1},
        opacity=0.3,
        row=1,
        col=1
    )

    fig.update_traces(
        customdata=tag_index_df['tag'],
        marker_line_width=2,
        hovertemplate= '<b>%{x}</b>: %{y:.3g}',
        name='Partisan Index',
        row=1, col=1
    )


    fig.add_traces(
        px.histogram(
            tag_status_df,
            x='tag',
            color='current_status',
            labels=gen_cfg['labels'],
            barmode='stack',
            category_orders=gen_cfg['category_orders'],
            color_discrete_map=color_maps['status_sequence'],
            hover_data=['current_status'],
        ).data,
        rows=2,cols=1
    )


    fig.for_each_annotation(
        lambda a: a.update(
            font={'size':16},
            hovertext=
            'For each topic, the <i>Partisan index</i> is<br>'
            '(<i># of Republican</i> - <i># of Democrat</i>) / (<i>Total #</i> bills)</i>'
            if 'Partisan' in a.text
            else "This is the breakdown of bills' <b>current status</b> for each topic",
            y=1.1
        )
    )

    fig.update_layout(layout_cfg)

    fig.update_layout(
        height=500,
        width=900,
        barmode='stack',
        yaxis1={
            'showline':True,
            'fixedrange':True,
            'ticks': None,
            'tickmode': 'array',
            'tickvals': [-1, 0, 1],
            'ticktext': ['<b>Democrat</b>', '<b>Bipartisan</b>', '<b>Republican</b>'],
            'tickfont': {'size':12},
        },
        xaxis1={
            'showline':True,
            'showgrid': True,
            'gridcolor': '#efefef',
            'gridwidth':1,
            'ticks':None,
            'showticklabels': False,
            'fixedrange':True,
            'tickmode':'linear'
        },
        xaxis2={
            'showline':True,
            'showgrid': True,
            'gridcolor': '#efefef',
            'gridwidth':1,
            'tickfont': {'size':10},
            'tickangle': -60,
            'ticks':None,
            'fixedrange':True,
            'tickmode':'linear'
        },
        yaxis2={
            'title_text': '# by Status'
        },
        legend={
            'orientation':"h",
            'yanchor':"top",
            'y':0.4,
            'xanchor':"center",
            'x':0.5,
            'font_size':9
        },
        margin={'pad': 1},
        # hovermode = 'y unified',
        spikedistance=0
    )
    return fig


@callback(
    Output(component_id='TAG_PARTISAN_BREAKDOWN', component_property='figure'),
    # Input(component_id='FOCUS_TAG_BREAKDOWN', component_property='hoverData'),
    Input(component_id='GLOBAL_TAG', component_property='value'),
)
def update_tag_figure(tag):
    # tag = hoverData['points'][0].get('customdata', 'Regulate Use')
    df = filter_by_tag(tag)

    co_tag = df['tags'].explode().value_counts()
    if tag.upper() != 'ALL':
        co_tag = co_tag.drop(tag)
    co_tag = {'<br>'.join(textwrap.TextWrapper(width=20).wrap(k)): v
              for k, v in co_tag.to_dict().items()}

    tagged_state = us_tilegrid.set_index('state_name')\
        .join(
        df['introducer_home_state'].value_counts().to_frame('num_tagged_bills')
    ).fillna({'num_tagged_bills':0}).reset_index()

    tagged_state['hover_desc'] =  tagged_state.apply(
        lambda x: "<b>%d</b> congressional bills introduced by<br>legislators from <b>%s</b><br>related to <i>%s</i>"
        %(x['num_tagged_bills'], x['state_name'], tag),
        axis=1
    )
    tagged_state = tagged_state.pivot(
        index='state_row',
        columns='state_column',
    )


    fig = make_subplots(
        rows=2,cols=2,
        shared_xaxes=False,
        shared_yaxes=False,
        column_widths=[1,1.5],
        row_heights=[1,3],
        horizontal_spacing=0.02,
        specs = [
            [{'type': 'xy'},{'type': 'xy', 'rowspan': 2}],
            [{'type': 'treemap',},{}]
        ],
        subplot_titles=[
            'Breakdown of Party support',
            'Which state do the congressional bill introducers come from?',
            'Related topics (co-ocurrence)'
        ]
    )


    fig.add_traces(
        px.histogram(
            df,
            y='party',
            color='party',
            color_discrete_map=color_maps['party'],
            category_orders=gen_cfg['category_orders'],
            labels=gen_cfg['labels'],
        ).data,
        rows=1,cols=1
    )

    fig.add_trace(go.Treemap(
        values = list(co_tag.values()),
        labels = list(co_tag.keys()),
        parents = [""] * len(co_tag),
        textinfo = "label+value+percent root",
        marker_colorscale='gray_r',
        root_color="white",
        name=''
    ),row = 2, col = 1)


    fig.add_trace(go.Heatmap(
        name='',
        z=tagged_state['num_tagged_bills'].fillna('').to_numpy(),
        colorscale = [[0, 'rgb(245,245,245)'], [1, 'rgb(10,10,10)']],
        hoverongaps=False,
    ), row=1,col=2)

    fig.update_layout(layout_cfg)


    fig.for_each_annotation(
        lambda a: a.update(
            font=dict(size=15)
        )
    )

    fig.update_traces(showlegend=False,row=1,col=1)
    fig.update_xaxes(ticks=None,row=1,col=1)
    fig.update_yaxes(ticks=None,showline=False, row=1,col=1)


    fig.update_xaxes(visible=False, row=1,col=2, constrain="domain", range=[-2,13], fixedrange=True)
    fig.update_yaxes(visible=False, row=1,col=2, scaleanchor='y', scaleratio=1, fixedrange=True)

    # fig.update_xaxes(visible=False, row=1,col=2, scaleanchor='y', scaleratio=1, fixedrange=True)
    # fig.update_yaxes(visible=False, row=1,col=2, constrain="domain", fixedrange=True)

    fig.update_traces(
        text=tagged_state['state_code'].fillna(''),
        texttemplate="%{text}",
        customdata=tagged_state['hover_desc'].fillna(''),
        hovertemplate='%{customdata}',
        row=1,col=2
    )

    fig.update_layout(
        title_text=f'Bills related to <b>{tag}</b>',
        title_font={'size':16},
        title_y=0.9,
        title_x=0.075,
        title_xanchor='left',
        margin={'pad': 5},
        height=450,
        width=1000,
        barmode='stack',
    )


    return fig


@callback(
    [
        Output('DISPLAY_TABLE', 'data'),
        Output('DISPLAY_TABLE', 'columns'),
        Output('LOADING_OUTPUT', 'children'),
    ],
    [Input('GLOBAL_TAG', 'value')]
)
def update_table(tag='ALL'):
    df = filter_by_tag(tag, display_df).rename(columns=gen_cfg['labels'])
    data = df.to_dict(orient='records')
    columns=[{"name": i, 'id': i, 'presentation': 'markdown'} if i == gen_cfg['labels']["bill_id"]
             else {"name": i, 'id': i}
             for i in df.columns]
    return data, columns, True

### 5. Create app ##################################################

app = Dash(
    name=__name__,
    title='Tech policy dashboard',
)

server = app.server

app.layout = html.Div([
    html.Div([
        html.H1(
            children='US Congressional Tech Policy "Tracker"',
            title= 'Note: Currently not updated live',
            style={
                'width': '30%',
                'min-width': '300px',
                'textAlign':'left',
                'display': 'inline-block'
            },
        ),
        html.Div(
            [
                html.H2(
                    "Global Topic filter",
                    className="var-title",
                    style={
                        "width": "20%",
                        'textAlign': 'left',
                        'display': 'inline-block',
                        'vertical-align': 'middle'
                    },
                    title= 'Use the dropdown to select a bill topic. "ALL" refers to everything, no filer'
                ),
                html.Div([
                    dcc.Dropdown(
                        id="GLOBAL_TAG",
                        options=all_tags,
                        value="ALL",
                        clearable=False
                    )],
                    style={
                        'width': '50%',
                        'font-size': '12pt',
                        'display': 'inline-block',
                        'font-weight': 'normal',
                        'horizontal-align': 'left',
                        'vertical-align': 'middle'
                    },
                ),
                html.H2(
                    "Scroll down for table details",
                    style={
                        'font-style': 'italic',
                        'font-weight': 'normal',
                        "width": "30%",
                        'textAlign': 'center',
                        'display': 'inline-block',
                        'vertical-align': 'middle'
                    },
                ),
            ],
            style={
                'width': '50%',
                'min-width': '500px',
                'textAlign':'left',
                'display': 'inline-block'
            },
        ),
        html.H2(
            children=[
                'Data source: ',
                html.A(
                    children='Citris Policy Lab',
                    href='https://citrispolicylab.org/ailegislation/',
                    title='Citris Policy Lab AI Legislation',
                ),
            ],
            style={
                'width': '20%',
                'min-width': '300px',
                'textAlign': 'right',
                'display': 'inline-block',
                'font-weight': 'normal',
            },
        ),
    ]),
    html.Hr(),
    dcc.Loading(
        id="LOADING_DISPLAY",
        type="default",
        children=html.Div(id="LOADING_OUTPUT")
    ),
    html.Div([
        dcc.Graph(
            id='TOTAL_BILLS_BY_STATUS',
            style={
                'width': '45%',
                'min-width': '750px',
                'display': 'inline-block',
                'vertical-align': 'middle'
            }
        ),
        html.Div([
                html.Div([
                    html.H3("Select options for distribution (right plot)", className="var-title"),
                    html.Hr(),
                    html.P("Status type", className="var-title"),
                    dcc.Dropdown(
                        id="BILLS_BY_INTRODUCED_DATE::status_type",
                        options=[
                            {'label': gen_cfg['labels'][k], 'value': k}
                            for k in [
                                'current_status',
                                'cumulative_status'
                            ]
                        ],
                        value="current_status",
                        clearable=False
                    ),
                    html.P("Color by", className="var-title"),
                    dcc.Dropdown(
                        id="BILLS_BY_INTRODUCED_DATE::color_by",
                        options=[
                            {'label': gen_cfg['labels'][k], 'value': k}
                            for k in [
                                'party',
                                'legislature_chamber'
                            ]
                        ],
                        value="party",
                        clearable=False
                    ),
                    html.Br(),
                    html.Br(),
                    html.Div([
                        daq.BooleanSwitch(
                            id="BILLS_BY_INTRODUCED_DATE::show_stack",
                            on=True,
                            label="Stack",
                            labelPosition='top',
                            style={ 'width': '50%', 'display': 'inline-block', 'horizontal-align': 'left'}
                        ),
                        daq.BooleanSwitch(
                            id="BILLS_BY_INTRODUCED_DATE::cum_hist",
                            on=False,
                            label="Cumulative",
                            labelPosition='top',
                            style={ 'width': '50%', 'display': 'inline-block', 'horizontal-align': 'right' }
                        ),
                        ],
                    )
                    ],
                    style={
                        # 'width': '10%',
                        'width': '20%',
                        'min-width': '200px',
                        'display': 'inline-block',
                        'vertical-align': 'middle',
                        'horizontal-align': 'left',
                    }
                ),
                dcc.Graph(
                    id='BILLS_BY_INTRODUCED_DATE',
                    style={
                        # 'width': '35%',
                        'width': '80%',
                        'min-width': '800px',
                        'display': 'inline-block',
                        'vertical-align': 'middle'
                    }
                ),
            ],
            style={
                'width': '45%',
                'min-width': '1000px',
                'display': 'inline-block'
            }
        )
    ]),
    html.Hr(),
    html.Div([
        dcc.Graph(
            id='FOCUS_TAG_BREAKDOWN',
            figure=create_fig_tag_breakdown(),
            # hoverData={'points': [{'customdata': 'Regulate Use'}]},
            style={
                'width': '45%',
                'min-width': '800px',
                'display': 'inline-block',
                'vertical-align': 'top'
            }
        ),
        dcc.Graph(
            id='TAG_PARTISAN_BREAKDOWN',
            style={
                'width': '55%',
                'min-width': '1000px',
                'display': 'inline-block',
                'vertical-align': 'top'
            }
        )
    ]),
    html.H2("Table details", title="This data underwent some light preprocessing from the original data"),
    dash_table.DataTable(
        id='DISPLAY_TABLE',
        style_as_list_view=True,
        style_data={
            'maxWidth': '100px',
            'overflow': 'hidden',
            'textOverflow': 'ellipsis',
            'whiteSpace': 'normal'
        },
        style_table={
            'overflowX': 'auto',

        },
        style_cell={
            'textAlign': 'left',
            'textOverflow': 'ellipsis'
        },
        style_header={
            'fontWeight': 'bold'
        },
        sort_action="native",
        sort_mode='multi',
        filter_action="native",
        filter_options={"placeholder_text": "Filter column ..."},
        page_action='native',
        page_current= 0,
        page_size= 10,
        markdown_options={"html": True},
    ),
    html.Br(),
    html.Hr(),
    dcc.Markdown(
        desc_md,
        className='desc',
        mathjax=True
    ),
    html.Br(),
    html.Hr(),
    html.H3(
        children=[
            'Source code: ',
            html.A(
                children='codeberg.org/penguinsfly/tech-policy-viz',
                href='https://codeberg.org/penguinsfly/tech-policy-viz',
                title='Source code',
            ),
        ],
        style={
            'textAlign': 'left',
            'font-weight': 'normal',
        }
    )
])


if __name__ == '__main__':
    app.run_server(debug=True)
