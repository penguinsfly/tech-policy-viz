# Visualization for Tech policy legislative progress in the US

[![status-badge](https://ci.codeberg.org/api/badges/penguinsfly/tech-policy-viz/status.svg)](https://ci.codeberg.org/penguinsfly/tech-policy-viz)

## Description

The dashboard is currently live at **<https://policy-dashboard.fly.dev>**

This creates a `dash` dashboard to visualize the "current" progress of federal/congressional tech policy in the United States, using data from [Citris Policy Lab](https://citrispolicylab.org/ailegislation/). This is hosted with [`fly.io`](https://fly.io/).

## Limitations

*On data & dashboard*: Currently the dashboard is only visualizing the **static** data without any updates from the original data sources, and at the moment there does not seem to be any updates on the Citris Policy Lab spreadhsheet.

*On display platform*: This is best viewed on desktop but I've configured so that it is possible to view on phone as well, though less optimal.

*On content*: I do not come from a policy/law background. My understanding is limited, and I am slowly learning. I am open to feedback on my visualization to better communicate effectively and accurately about this topic. Feel free to open an issue!

## Requirements

- `python`
- `dash`
- `plotly`
- `make` (optional)

For more detailed list, see the [`requirements.txt`](./requirements.txt) file.



## Data

- The policy tracker data are obtained from the [Citris Policy Lab](https://citrispolicylab.org/ailegislation/)
- The US state tile grid data file is obtained from [Kaggle](https://www.kaggle.com/datasets/poznyakovskiy/us-tile-grid)


## Run

### Locally

Install requirements:

```bash
pip install -r requirements.txt  # or `make install`
```

Then run:

```bash
DASH_HOT_RELOAD=true python app.py # or `make run-debug`
# then open http://127.0.0.1:XXXX/ in browser
```

### Deploy to `fly.io`

This is deploy with `fly.io`:

```bash
flyctl deploy
```

This is currently deployed via Codeberg CI with `fly.io`.

## Acknowledgements

Thanks Adam Bacigal for helping me throughout the planning, sketching and advice of this visualization, and the DVS mentorship for the opportunity to learn!
Also, thanks Dr. Suresh Venkatasubramanian & Sasa Jovanovic for feedback.
