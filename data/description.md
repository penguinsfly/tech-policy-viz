### Additional information

The 4 top left panels show the overall summary of the number of bills at various different progresses of the federal legislation.
The bills go through 4 different stages: *Introduced* (either in House (left panels) or Senate (right panels)),
*Pass First Chamber*, *Pass Other Chamber* then finally *Became Law*.
These plots show either the **current status** (top panels)
or **minimum status** (i.e. the status at which a given bill at least have passed, bottom panels).
The plots also show breakdown of the party support (Bipartisan, Democrat, Republican).

The 4 top right panels show the bill distributions based on the date at which bills are introduced,
broken down by either party support or the chamber bills are introduced in.
The controls on the left of these panels give the option to choose between whether to plot
**current** or **minimum status**, and whether to show in cumulative mode.

The 2 bottom left panels show the breakdown of each bill topic (a bill can have various topics)
based on the bipartisan support (left) or by the current status (right).
The bill topic selection is at the top filter.

The *partisan index* of a given topic $\mathbf{t}$ is defined as:

$$
\mathtt{partisan}(\mathbf{t})
=
\frac{
(-1) \times N_{Democrat}(\mathbf{t}) +
(+1) \times N_{Republican}(\mathbf{t})
}{
N(\mathbf{t}) =
N_{Democrat}(\mathbf{t}) +
N_{Republican}(\mathbf{t}) +
N_{Bipartisan}(\mathbf{t})
}
$$

The bottom right panels show the breakdown of a given topic by partisan support (top),
relevant co-occurring topics (bottom) and by the state which the bill introducers come from (right).

### Additional notes on data

These are the following amendments made to the original data:
