import os
import re

import numpy as np
import pandas as pd
import textwrap
import yaml

def prepare_description(text, width=50):
    return '<br>'.join(textwrap.TextWrapper(width=width).wrap(text))


def __norm_year__(year, prepend='20'):
    # this turns 2-digit year to 4-digit year
    year = str(year)
    if len(year) == 2:
        year = prepend + year
    assert len(year) == 4
    return year


def merge_tags_duplicated_bills(df):
    if len(df) == 1:
        return df.drop(columns='unq_id')

    assert len(df) == 2
    tags = list(df['tags'].explode().unique())
    df = df.iloc[0]
    df['tags'] = tags
    df['update_info'] = '- Issue: Found duplicate bill ID.\n- Action: Merge focus tags.'
    return pd.DataFrame([df]).drop(columns='unq_id')


def infer_session(row, state_default='CA', congress_default='US'):
    is_congress = row['is_congress']

    status = [row[x] for x in list(row.keys()) if x.lower().startswith('status')]
    assert len(status) == 2 and pd.isna(status).sum() == 1
    status = [s for s in status if not pd.isna(s)][0]

    if is_congress:
        session_id = re.match('(^\d+)', row['Congress'].strip()).groups()
        assert len(session_id) == 1
        session_id = str(session_id[0])
        session_state = congress_default
    else:
        session_id = re.match('(\d+)[^\d]+(\d+)', row['California Session']).groups()
        assert len(session_id) == 2
        session_id = ''.join(sorted(map(__norm_year__, session_id)))
        session_state = state_default

    return dict(
        session = session_id,
        state = session_state,
        status = status
    )


def cumulative_status(row, status_order):
    introduced_chamber = row['legislature_chamber']
    status = row['status']

    is_passed_b4law = 'passed' in status.lower()
    if is_passed_b4law:
        chamber_passed = status.lower().replace('passed', '').strip()
        if chamber_passed == introduced_chamber.lower():
            status = 'Passed First Chamber'
        else:
            status = 'Passed Other Chamber'

    order = status_order[status]+1
    return list(np.array(list(status_order.keys()))[:order])


def load_congressional_legislation(
    file,
    row_update=dict(),
    status_order=dict(),
    status_update=dict(),
    fill_val_for_missing_bipartisan_support='No',
    maxchar_oldval=50,
):

    # Load data
    cpl_df = pd.read_csv(
        file,
        skip_blank_lines=False,
    ).dropna(how='all').reset_index(drop=True)
        
    # Fix rows
    cpl_df['update_info'] = np.nan
    for title2look, update_dict in row_update.items():
        idx = cpl_df.query('Title == @title2look').index
        assert len(idx) == 1
        idx = list(idx)[0]

        info = ''
        note = update_dict.pop('note', None)
        for key, new_val in update_dict.items():
            old_val = str(cpl_df.loc[idx, key])
            if len(old_val) > maxchar_oldval:
                old_val = old_val[:maxchar_oldval] + '...'
            cpl_df.loc[idx, key] = new_val
            info += f'- Action: Update *{key}* from *{old_val}* to *{new_val}*\n'
        
        if note:
            info += f'- {note}\n'
            
        cpl_df.loc[idx, 'update_info'] = info
        
    # Process date
    cpl_df['created_date'] = pd.to_datetime(
        cpl_df['Date Introduced'],
        dayfirst=False,
        yearfirst=False
    )

    # Process Bill IDs
    cpl_df['bill_id'] = cpl_df['Title'].apply(
        lambda x: re.findall(
            '[A-Z]+\d+',
            re.sub('[^\w]', '', x.split(':')[0].upper()).strip()
        )
    ).apply(
        lambda x: None if len(x) == 0 else x[0]
    )
    
    cpl_df = cpl_df.dropna(subset='bill_id').reset_index(drop=True)

    # Prepare description
    cpl_df['bill_description'] = cpl_df['Title']
    cpl_df['bill_desc_hover'] = cpl_df['bill_description'].apply(prepare_description)

    # Fix weird values
    cpl_df.loc[
        ~cpl_df['California Assembly or Senate'].str.lower().isin(['assembly', 'senate']),
        'California Assembly or Senate'
    ] = None
    
    # Asserting that there are no bills that are both Congress and State
    assert all(pd.isna(cpl_df['US House or Senate']) != pd.isna(cpl_df['California Assembly or Senate']))
    assert all(pd.isna(cpl_df['Congress']) != pd.isna(cpl_df['California Session']))
    assert all(pd.isna(cpl_df['Congress']) == pd.isna(cpl_df['US House or Senate']))
    assert all(pd.isna(cpl_df['Status (California)']) != pd.isna(cpl_df['Status (Federal)']))
    assert all(pd.isna(cpl_df['US House or Senate']) == pd.isna(cpl_df['Status (Federal)']))
    assert all(pd.isna(cpl_df['California Assembly or Senate']) == pd.isna(cpl_df['Status (California)']))
    cpl_df['is_congress'] = pd.isna(cpl_df['California Assembly or Senate'])

    # Session
    cpl_df = pd.concat([
        cpl_df,
        pd.json_normalize(cpl_df.apply(infer_session, axis=1))
    ], axis=1)

    # Unique ID from session and bill ID
    cpl_df['unq_id'] = cpl_df.apply(
        lambda row: '_'.join([
            row.get(k) for k in
            ['state', 'session', 'bill_id']
        ]),
        axis=1
    )

    # ONLY OBTAIN CONGRESSIONAL BILLS
    cpl_df = cpl_df.query('is_congress == True').reset_index(drop=True)

    # Missing bipartisan support field
    cpl_df = cpl_df.fillna({'Bipartisan Support': fill_val_for_missing_bipartisan_support})

    # Preprocess some text columns
    cpl_df['legislature_chamber'] = cpl_df['US House or Senate'].apply(lambda x: x.strip())
    cpl_df['introducer_home_state'] = cpl_df['Home State of Introducer'].apply(lambda x: x.strip())
    cpl_df['introducer_party'] = cpl_df['Party Affiliation of Introducer'].apply(lambda x: x.strip())
    cpl_df['is_bipartisan'] = cpl_df['Bipartisan Support'].str.upper() == 'YES'

    # Tags
    cpl_df['tags'] = cpl_df['Focus Tags'].fillna('NO_TAG').apply(lambda x: list(set(x.split(','))))

    # Website: lower-case to keep
    cpl_df = cpl_df.rename(columns={'Website': 'website'})

    # Only keep certain columns
    selected_cols = cpl_df.columns[cpl_df.columns.map(lambda x: x.islower())]
    cpl_df = cpl_df.filter(selected_cols)

    # Drop duplicates
    cpl_df = cpl_df.groupby('unq_id').apply(merge_tags_duplicated_bills).reset_index()

    # Tag counts for convenience
    cpl_df['tag_hover'] = cpl_df['tags'].apply(lambda x: '|'.join(x)).apply(prepare_description)
    cpl_df['tag_count'] = cpl_df['tags'].apply(lambda x: np.ones((len(x),)))
    cpl_df['tag_weight'] = cpl_df['tag_count'].apply(lambda x: x / len(x))

    # Compute party support
    cpl_df['party'] = cpl_df.apply(
        lambda x:
        'Bipartisan' if x['is_bipartisan']
        else x['introducer_party'],
        axis=1
    )

    # Status update
    for _unq_id, status_update_info in status_update.items():
        new_status = status_update_info['new_status']
        extra_notes = status_update_info.get('note', '')

        id_loc = cpl_df['unq_id'] == _unq_id
        old_status = cpl_df[id_loc]['status'].to_list()
        assert len(old_status) == 1
        old_status = old_status[0]

        info = f'- Action: Update status *{old_status}* to *{new_status}*'
        if len(extra_notes) > 0:
            info = info + '\n' + extra_notes

        cpl_df.loc[id_loc, 'status'] = new_status
        cpl_df.loc[id_loc, 'update_info'] = info

    # Cumulative status and Current status "normalization"
    cpl_df['cumulative_status'] = cpl_df.apply(
        lambda x: cumulative_status(x, status_order),
        axis=1
    )
    cpl_df['cumulative_status_order'] = cpl_df['cumulative_status'].apply(
        lambda x: [status_order[xi] for xi in x]
    )
    cpl_df['current_status'] = cpl_df['cumulative_status'].apply(lambda x: x[-1])
    cpl_df['current_status_order'] = cpl_df['cumulative_status_order'].apply(lambda x: x[-1])

    update_info = cpl_df.query('not update_info.isnull()').filter(['unq_id', 'update_info']).reset_index(drop=True)
    cpl_df = cpl_df.drop(columns='update_info')
    update_info = update_info.apply(
        lambda x: f"**{x['unq_id']}**\n{x['update_info']}\n",
        axis=1
    )
    update_info = '\n'.join(update_info.to_list())
    return cpl_df, update_info


def tabulate_bills(
    df, variables, source,
    id_column='unq_id', metric_name='num_bills',
    var_name='status_type', value_name='status',
    template_df=None
):
    if source not in df and isinstance(template_df, pd.DataFrame):
        template_df = template_df.copy()
        template_df[var_name] = source
        return template_df

    if type(df[source][0]) in [list, np.array]:
        df = df.explode(source)
    df = df.groupby(variables + [source])\
        [id_column].apply(lambda x: len(np.unique(x)))\
        .to_frame(metric_name)\
        .reset_index()\
        .melt(
            id_vars=variables + [metric_name],
            var_name=var_name,
            value_name=value_name
        )
    return df


def tabulate_tags(cpl_df):
    partisan_index = {
        'Democrat': -1,
        'Bipartisan': 0,
        'Republican': +1
    }

    tag_index_df = cpl_df.explode(['tags', 'tag_count', 'tag_weight'])\
        .reset_index(drop=True)\
        .rename(columns={'tags':'tag'})\
        .filter(['tag', 'tag_count', 'party','tag_weight'])
    tag_index_df['partisan_index'] = tag_index_df['party'].map(partisan_index)

    tag_index_df['partisan_weight'] = tag_index_df['partisan_index'] * tag_index_df['tag_weight']

    tag_index_df = tag_index_df.drop(columns='party').groupby('tag')\
        .agg(['sum']).reset_index().droplevel(level=1,axis=1)
    tag_index_df['partisan_index'] = tag_index_df['partisan_index'] / tag_index_df['tag_count']
    tag_index_df['partisan_weight'] = tag_index_df['partisan_weight'] / tag_index_df['tag_weight']

    tag_status_df = cpl_df.explode(['tags', 'tag_count'])\
        .reset_index(drop=True)\
        .rename(columns={'tags':'tag'})\
        .filter(['tag', 'current_status'])

    return tag_index_df,tag_status_df

