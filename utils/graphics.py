import yaml
import matplotlib as mpl
import plotly.graph_objects as go 


def load_layout_config(file_name, axis_ids=10):
    with open(file_name, 'r') as f:
        cfg = yaml.safe_load(f)

    axis_configs = {}
    if isinstance(axis_ids, int):
        axis_ids = range(axis_ids)
        
    for i in axis_ids:
        axis_id = '%d' %(i) if i > 0 else ''
        axis_configs['xaxis' + axis_id] = cfg['axis']
        axis_configs['yaxis' + axis_id] = cfg['axis']

    layout = go.Layout(
        **axis_configs,
        font=cfg['font'],
        **cfg['general'],
        **cfg['title']
    )
    
    return layout


def load_traces_config(file_name):
    with open(file_name, 'r') as f:
        cfg = yaml.safe_load(f)
    return cfg


def offset_signal(signal, marker_offset):
    # Source:
    # - https://gist.github.com/caiotaniguchi/2031c1614e6b8aa142198fe548c4b77f
    # - https://medium.com/@caiotaniguchi/plotting-lollipop-charts-with-plotly-8925d10a3795
    if abs(signal) <= marker_offset:
        return 0
    return signal - marker_offset if signal > 0 else signal + marker_offset


def plot_lollipop(
    df, x, y, 
    orientation = 'vertical',
    sort_by=None, 
    ascending=True,
    hue=None,
    x_base = 0, 
    y_base = 0,
    cmin = -1,
    cmax = 1,
    showscale = True,
    cmap = 'RdBu_r',
    marker_offset = 0.01,
    line_width = 2,
    marker_size = 10,
    line_color=None,
    base_color='black',
    base_linewidth = 1,
    fig=None,
    fig_kwargs=dict()
): 
    # Inspired by:
    # - https://gist.github.com/caiotaniguchi/2031c1614e6b8aa142198fe548c4b77f
    # - https://medium.com/@caiotaniguchi/plotting-lollipop-charts-with-plotly-8925d10a3795

    is_vertical = orientation == 'vertical'

    x_data = df[x]
    y_data = df[y]

    hue_data = df[hue]

    norm = mpl.colors.Normalize(vmin=cmin, vmax=cmax)
    if isinstance(cmap, list):
        cmap_obj = mpl.colors.ListedColormap(cmap)
    elif isinstance(cmap, str):
        cmap_obj = mpl.colormaps.get_cmap(cmap)

    colors = ['rgba' + str(cmap_obj(norm(x))) for x in hue_data]

    data = [
        go.Scatter(
            x=x_data,
            y=y_data,
            mode='markers',
            marker=dict(
                size=marker_size,
                color=hue_data,
                showscale=showscale,
                colorscale=cmap
            ),
            showlegend=False,
        )
    ]
    
    shapes = [
        dict(
            type='line',
            xref='x', 
            yref='y',
            x0=x_val if is_vertical else x_base,
            y0=y_base if is_vertical else y_val,
            x1=x_val if is_vertical else offset_signal(x_val, marker_offset=marker_offset),
            y1=offset_signal(y_val, marker_offset=marker_offset) if is_vertical else y_val,
            line=dict(
                width=line_width,
                color=line_color if line_color is not None else color,                
            )
        )
        for x_val, y_val, color in zip(x_data, y_data, colors)
    ]
    
    
    layout = go.Layout(
        shapes=shapes,
    )
    
    if fig is None:
        fig = go.Figure(data, layout)
    else:
        fig.add_traces(data, **fig_kwargs)
        fig.update_layout(layout)
        
    if sort_by is not None:
        cat_order = df.sort_values(by=sort_by, ascending=ascending)
        if is_vertical:
            fig.update_xaxes(
                categoryorder='array',
                categoryarray=cat_order[x]
            )
        else:
            fig.update_yaxes(
                categoryorder='array',
                categoryarray=cat_order[y]
            )
            
    return fig
