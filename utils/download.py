import os
import time

from selenium import webdriver
from selenium.webdriver.firefox.service import Service as FirefoxService
from selenium.webdriver.firefox.options import Options as FirefoxOptions

from selenium.webdriver.common.by import By

path = os.getcwd()

DOWNLOAD_DIRECTORY = os.path.join(path, 'download')
AIRTABLE_URL='https://airtable.com/shrb65Ssk4zgDGneP'
COMMAND_EXECUTOR='http://selenium_firefox:4444/wd/hub'
NECESSARY_DOWNLOADED_FILE='AI Legislation-Grid view.csv'


print(f'1. Setting up headless browser and download directory to {DOWNLOAD_DIRECTORY}', end='')
options = FirefoxOptions()
options.add_argument('--headless')
options.set_preference("browser.download.folderList", 2)
options.set_preference("browser.download.manager.showWhenStarting", False)
options.set_preference("browser.download.dir", DOWNLOAD_DIRECTORY)
options.set_preference("browser.helperApps.neverAsk.saveToDisk", "image/png")
options.set_preference("browser.helperApps.neverAsk.saveToDisk", "text/csv")

driver = webdriver.Remote(
    command_executor=COMMAND_EXECUTOR,
    options=options
)
print('\t ... done')


print(f'2. Going to website <{AIRTABLE_URL}>', end='')
driver.get(AIRTABLE_URL)
time.sleep(1)
print('\t ... done')


print('3. Clicking on menu button to review download options', end='')
button = driver.find_element(by=By.CLASS_NAME, value='viewMenuButton')
button.click()
time.sleep(1)
print('\t ... done')


print('4. Clicking on "Download as CSV" button', end='')
csv_button = driver.find_element(
    by=By.CSS_SELECTOR,
    value='[data-tutorial-selector-id="viewMenuItem-viewExportCsv"]'
)
csv_button.click()
time.sleep(10)
print('\t ... done')


print(f'5. Checking in download directory {DOWNLOAD_DIRECTORY} ...')
files = os.listdir(DOWNLOAD_DIRECTORY)
print('\t Downloaded files:')
print(files)
assert len(files) >= 0, f'No files found in {DOWNLOAD_DIRECTORY}'
assert NECESSARY_DOWNLOADED_FILE in files, f'The file {NECESSARY_DOWNLOADED_FILE} was not found'
print('\t ... download was success!')

print('SUCCESS WITH DOWNLOADING!')

