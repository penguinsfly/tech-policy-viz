#!/bin/bash

DATA_FILE='data/AI Legislation-Grid view.csv'
DOWNLOAD_FILE='download/AI Legislation-Grid view.csv'

diffcheck="$(diff --suppress-blank-empty --suppress-common-lines "$DATA_FILE" "$DOWNLOAD_FILE")"
exit_code=$?

if [[ $exit_code == 0 ]] ; then
  echo "The files in repository and newly downloaded are the SAME"
else
  echo "The files in repository and newly downloaded are the DIFFERENT"
  exit 1
fi

