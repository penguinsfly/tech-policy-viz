install:
	pip install -r requirements.txt
run-debug:
	DASH_HOT_RELOAD=true python app.py
run-gunicorn:
	gunicorn app:server
run:
	FLASK_APP=app flask run
deploy:
	flyctl deploy
